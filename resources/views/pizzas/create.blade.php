@extends('layouts.app')

@section('content')
<div class="wrapper create-pizza">
      <h1>Create a New Pizza</h1>
      <form action="/pizzas" method="POST">
            @csrf
            <label for="" style=" margin-top: 30px;">Your name:</label>
            <input type="text" id="name" name="name"><br>
            <label for="type" style="margin-top: 10px;">Choose pizza type:</label>
            <select name="type" id="type" style="margin-top: 10px; padding: 8px; width: 200px;">
                  <option value="margarita">Margarita</option>
                  <option value="hawaiian">Hawaiian</option>
                  <option value="veg supreme">Veg Supreme</option>
                  <option value="volcano">Volcano</option>
            </select> <br>
            <label for="base" >Choose base type:</label>
            <select name="base" id="base" style="margin-top: 10px; padding: 8px; width: 200px;">
                  <option value="cheesy crust">Cheesy Crust</option>
                  <option value="garlic crust">Garlic Crust</option>
                  <option value="thin & crispy">Thin & Crispy</option>
                  <option value="thick">Thick</option>
            </select>
            <fieldset style="display: block; margin: 10px">
                  <label for="">Extra toppings:</label> <br>
                  <input type="checkbox" name="toppings[]" value="mushrooms">Mushrooms <br>
                  <input type="checkbox" name="toppings[]" value="peppers">Peppers <br>
                  <input type="checkbox" name="toppings[]" value="garlic">Garlic <br>
                  <input type="checkbox" name="toppings[]" value="olives">Olives <br>
            </fieldset>
            <div class="submit">
            <input type="submit" value="Order Pizza" style="background-color: #952121; color:#fff; border: 0; padding: 8px 20px; margin-top: 20px;" >
            </div>
           
      </form>
</div>
@endsection