@extends('layouts.app')

@section('content')
<div class="wrapper pizza-index">
  <h1>Pizza Orders</h1>
    @foreach($pizzas as $pizza)
      <div class="pizza-item" style="margin: 10px 0; padding: 12px; background: #f4f4f4;">
        <img src="/img/pizzaicon.png" alt="pizza icon" style="width: 90px; display: inline-block;">
        <h4 style="display: inline-block; font-weight: normal; margin-left: 20px;"><a href="/pizzas/{{ $pizza->id }}" style="color: #777; text-decoration: none !important;"> {{ $pizza->name }} </a></h4>
      </div>
    @endforeach
</div>

@endsection
