<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pizza;
use App\Models\Pizzahouse;
use Illuminate\Support\Facades\DB;

class PizzaController extends Controller
{
  // public function __construct(){
    // $this->middleware('auth');
  // }
  public function index() {
    $pizzas = Pizzahouse::all();
    // $pizzas = DB::table('pizza')->where('id','>',1)->get();
    // $pizzas = Pizza::orderBy('name', 'desc')->get();
    // $pizzas = Pizza::where('type', 'hawaiian')->get();
    // $pizzas = Pizza::latest()->get();
    $data = [
        'pizzas'=>$pizzas
    ];
    return view('pizzas.index', $data);

    // return view('pizzas', compact('pizzas'));
  }

  public function show($id) {
    // use the $id variable to query the db for a record

    $pizza = Pizzahouse::findOrfail($id);

    return view('pizzas.show', ['pizza' => $pizza]);
  }

  public function create(){
    return view('pizzas.create');
  }

  public function store(){

    $pizza = new Pizzahouse();

    $pizza->name = request('name');
    $pizza->type = request('type');
    $pizza->base = request('base');
    $pizza->toppings = request('toppings');

    $pizza->save();

    return redirect('/')->with('mssg', 'Thank for your order');
  }

  public function destory($id){
    $pizza = Pizzahouse::findOrFail($id);
    $pizza->delete();

    return redirect('/pizzas');
  }

}
